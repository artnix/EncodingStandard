﻿namespace EncodingStandard
{
    public interface IEncoder
    {
        string AnsiToUnicode(string text);
        string UnicodeToAnsi(string text);
    }
}