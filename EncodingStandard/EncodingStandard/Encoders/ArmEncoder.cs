﻿using System.Text;

namespace EncodingStandard.Encoders
{
    internal class ArmEncoder : IEncoder
    {
        public string UnicodeToAnsi(string text)
        {
            var builder = new StringBuilder();
            foreach (char symbol in text)
            {
                switch (GetAnsiNumber(symbol))
                {
                    case 1415: //ev
                        builder.Append((char) 168);
                        break;

                    case 1329: // TODO: to 1366
                        builder.Append((char)((symbol - 1240) * 2));
                        break;

                    case 1377: // TODO: to 1414
                        builder.Append((char)((symbol - 1288) * 2 + 1));
                        break;

                    case 1373: //`
                        builder.Append((char) 170);
                        break;

                    case 171: // <<
                        builder.Append((char)167);
                        break;

                    case 187: // >>
                        builder.Append((char)166);
                        break;

                    case 1371: // shesht
                        builder.Append((char)176);
                        break;

                    case 1374: // ?
                        builder.Append((char)177);
                        break;

                    default:
                        builder.Append(symbol);
                        break;
                }
            }

            return builder.ToString();
        }

        public string AnsiToUnicode(string text)
        {
            var builder = new StringBuilder();
            foreach (char symbol in text)
            {
                switch (GetUnicodeNumber(symbol))
                {
                    case 168: //ev
                        builder.Append((char)1415);
                        break;

                    case 178: // TODO: to 253
                        builder.Append((char)(symbol % 2 * 48 + (symbol - 178) / 2 + 1329));
                        break;

                    case 170: //`
                        builder.Append((char)1373);
                        break;

                    case 167: // <<
                        builder.Append((char)171);
                        break;

                    case 166: // >>
                        builder.Append((char)187);
                        break;

                    case 176: // shesht
                        builder.Append((char)1371);
                        break;

                    case 177: // ?
                        builder.Append((char)1374);
                        break;

                    default:
                        builder.Append(symbol);
                        break;
                }
            }

            return builder.ToString();
        }

        private static int GetUnicodeNumber(char symbol)
        {
            if (symbol >= 178 && symbol <= 253)
                return 178;

            return symbol;
        }

        private static int GetAnsiNumber(char symbol)
        {
            if (symbol >= 1377 && symbol <= 1414)
                return 1377;

            if (symbol >= 1329 && symbol <= 1366)
                return 1329;

            return symbol;
        }

        private static int GetNumber(char ch)
        {
            int a = (int)ch;

            if (a >= 178 && a <= 253)
                return 178;

            if (a >= 1377 && a <= 1414)
                return 1377;

            if (a >= 1329 && a <= 1366)
                return 1329;

            return a;
        }
    }
}