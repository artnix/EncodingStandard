﻿using EncodingStandard.Encoders;

namespace EncodingStandard
{
    public static class CultureEncoder
    {
        public static IEncoder GetArmenianEncoder() => new ArmEncoder();
    }
}
